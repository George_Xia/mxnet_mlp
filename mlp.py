import mxnet as mx
import numpy as np
import struct
import time

##create the network
def net():
	data = mx.symbol.Variable('data')
	fc1 = mx.symbol.FullyConnected(data, name='fc1', num_hidden=128)
	act1 = mx.symbol.Activation(fc1, name='act1', act_type='relu')
	fc2 = mx.symbol.FullyConnected(act1, name='fc2', num_hidden=128)
	act2 = mx.symbol.Activation(fc2, name='act2', act_type='relu')
	fc3 = mx.symbol.FullyConnected(act2, name='fc3', num_hidden=10)
	softmax = mx.symbol.SoftmaxOutput(fc3, name='sm')
	return softmax

def generate_img(path):
	binfile = open(path)
	buf = binfile.read()
	index = struct.calcsize('>IIII')
	img = struct.unpack_from('>' + str(50000*784) + 'B', buf, index)
	img = np.array(img)
	train = img.reshape(-1, 784)
	index += struct.calcsize('>' + str(50000*784) + 'B')

	img = struct.unpack_from('>' + str(10000*784) + 'B', buf, index)
	img = np.array(img)
	val = img.reshape(-1, 784)
	return train/np.float32(256), val/np.float32(256)

def generate_label(path):
	binfile = open(path)
	buf = binfile.read()
	index = struct.calcsize('>II')
	label = struct.unpack_from('>50000B', buf, index)
	train = np.array(label)
	index += struct.calcsize('>50000B')

	label = struct.unpack_from('>10000B', buf, index)
	val = np.array(label)
	return train/np.float32(256), val/np.float32(256)

if __name__ == '__main__':
	##load the data
	train_data = '../data/train-images-idx3-ubyte'
	train_label = '../data/train-labels-idx1-ubyte'
	test_data = '../data/t10k-images-idx3-ubyte'
	test_label = '../data/t10k-labels-idx1-ubyte'

	#prepare data
	train, val = generate_img(train_data)
	train_label, val_label = generate_label(train_label)
	print train[0]
	print len(train[0])
	print train.shape

	softmax = net()
	print softmax.list_arguments()
	print softmax.infer_shape(data=(50000, 784))

	##prepare data
	data = mx.nd.array(train)
	fc1_weight_0 = np.random.uniform(-1, 1, (128, 784))
	fc1_bias_0 = np.random.uniform(-1, 1, (128,))
	fc2_weight_0 = np.random.uniform(-1, 1, (128, 128))
	fc2_bias_0 = np.random.uniform(-1, 1, (128,))
	fc3_weight_0 = np.random.uniform(-1, 1, (10, 128))
	fc3_bias_0 = np.random.uniform(-1, 1, (10,))

	fc1_weight_1 = np.zeros((128, 784))
	fc1_bias_1 = np.zeros((128,))
	fc2_weight_1 = np.zeros((128, 128))
	fc2_bias_1 = np.zeros((128,))
	fc3_weight_1 = np.zeros((10, 128))
	fc3_bias_1 = np.zeros((10,))
	#sm_label = mx.nd.array(train_label)
	#sm_label = np.random.randint(0, 1, (50000,))
	##transfer the data into ndarray
	fc1_weight_0 = mx.nd.array(fc1_weight_0)
	fc1_bias_0 = mx.nd.array(fc1_bias_0)
	fc2_weight_0 = mx.nd.array(fc2_weight_0)
	fc2_bias_0 = mx.nd.array(fc2_bias_0)
	fc3_weight_0 = mx.nd.array(fc3_weight_0)
	fc3_bias_0 = mx.nd.array(fc3_bias_0)
	sm_label_0 = mx.nd.array(train_label)

	fc1_weight_1 = mx.nd.array(fc1_weight_1)
	fc1_bias_1 = mx.nd.array(fc1_bias_1)
	fc2_weight_1 = mx.nd.array(fc2_weight_1)
	fc2_bias_1 = mx.nd.array(fc2_bias_1)
	fc3_weight_1 = mx.nd.array(fc3_weight_1)
	fc3_bias_1 = mx.nd.array(fc3_bias_1)

	args = {'data':data, 'fc1_weight':fc1_weight_0, 'fc1_bias':fc1_bias_0, 'fc2_weight':fc2_weight_0, 'fc2_bias':fc2_bias_0,
			'fc3_weight':fc3_weight_0, 'fc3_bias':fc3_bias_0, 'sm_label':sm_label_0}

	#args_grad = args.copy()
	args_grad = {'fc1_weight':fc1_weight_1, 'fc1_bias':fc1_bias_1, 'fc2_weight':fc2_weight_1, 'fc2_bias':fc2_bias_1,
			'fc3_weight':fc3_weight_1, 'fc3_bias':fc3_bias_1}
	print args_grad['fc1_bias'].asnumpy()

	##training the model
	exe = softmax.bind(ctx=mx.context.cpu(), args=args, args_grad=args_grad)
	start_time = time.time()
	for i in range(30):
		exe.forward()
		exe.backward()
	mx.nd.waitall()
	print time.time()-start_time

	print args_grad['fc1_bias'].asnumpy()
